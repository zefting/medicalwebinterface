import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NewStepComponent } from '../new-step/new-step.component';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css'],
})
export class LandingPageComponent implements OnInit {
  public eventIsCollapsed = true;
  public medicinIsCollapsed = true;
  constructor(private modalService: NgbModal) {}
  names = [
    { id: '11', name: 'John Doe' },
    { id: '10', name: 'Jane Doe' },
  ];
  patients = [
    {
      patientBiologicalGender: '',
      patientName: '',
      patinentNumber: '',
      address: {
        city: '',
        streetname: '',
        zipCode: '',
        country: '',
      },
      primaryDoctor: {
        pracis: '',
        city: '',
        streetname: '',
        zipCode: '',
        country: '',
      },
      events: [
        {
          eventName: '',
          treatingDoctor: '',
          start: '',
          end: '',
        },
      ],
      medicins: [
        {
          medicationName: '',
          dose: '',
          proscribedBy: '',
        },
      ],
    },
  ];
  selectedpatient = {
    patientBiologicalGender: '',
    patientName: '',
    patinentNumber: '',
    address: {
      city: '',
      streetname: '',
      zipCode: '',
      country: '',
    },
    primaryDoctor: {
      pracis: '',
      city: '',
      streetname: '',
      zipCode: '',
      country: '',
    },
    events: [
      {
        eventName: '',
        treatingDoctor: '',
        start: '',
        end: '',
      },
    ],
    medicins: [
      {
        medicationName: '',
        dose: '',
        proscribedBy: '',
      },
    ],
  };
  doctorId = 1;
  newEvents = [];
  treatmentPlan = [];
  /* in this example, local data is used*/
  findPatient(id) {
    this.selectedpatient = this.patients.find((x) => x.patinentNumber === id);
    console.log(this.selectedpatient);
  }
  removeEvent(i) {
    this.newEvents.splice(i, 1);
  }
  addEvent() {
    const date = new Date();
    const eventObj = {
      doctorId: this.doctorId,
      eventDes: '',
      start: {},
    };
    this.newEvents.push(eventObj);
  }
  addTreamentStep() {
    const treatmentObj = {
      description: '',
      treatmenttype: '',
      bodylocation: '',
    };
    this.treatmentPlan.push(treatmentObj);
  }
  removeTreamentStep(i) {
    this.treatmentPlan.splice(i, 1);
  }
  ngOnInit(): void {
    this.patients = [
      {
        patientBiologicalGender: '1',
        patientName: 'Jane Doe',
        patinentNumber: '10',
        address: {
          city: 'Copenhagen',
          streetname: 'Prinsessegade 10',
          zipCode: '1000',
          country: 'Denmark',
        },
        primaryDoctor: {
          pracis: 'Doctor 1',
          city: 'Copenhagen',
          streetname: 'Doctor Street 20',
          zipCode: '1000',
          country: 'Denmark',
        },
        events: [
          {
            eventName: 'Admitted to hospital',
            treatingDoctor: 'James Jameson',
            start: '11-10-2020',
            end: '11-10-2020',
          },
        ],
        medicins: [
          {
            medicationName: 'Medicin1',
            dose: '10 mg',
            proscribedBy: 'Jane Janesen',
          },
        ],
      },
      {
        patientBiologicalGender: '0',
        patientName: 'John Doe',
        patinentNumber: '11',
        address: {
          city: 'Copenhagen',
          streetname: 'Prinsessegade 10',
          zipCode: '1000',
          country: 'Denmark',
        },
        primaryDoctor: {
          pracis: 'Doctor 1',
          city: 'Copenhagen',
          streetname: 'Doctor Street 20',
          zipCode: '1000',
          country: 'Denmark',
        },
        events: [
          {
            eventName: 'Admitted to hospital',
            treatingDoctor: 'James Jameson',
            start: '11-10-2020',
            end: '11-10-2020',
          },
        ],
        medicins: [
          {
            medicationName: 'Medicin1',
            dose: '10 mg',
            proscribedBy: 'Jane Janesen',
          },
        ],
      },
    ];
    this.treatmentPlan = [
      {
        stepId: 1,
        description: 'ipsum lorem',
        treatmenttype: 'Check up',
        bodylocation: 'Upper leg',
      },
      {
        stepId: 2,
        description: 'ipsum lorem',
        treatmenttype: 'Consultation',
        bodylocation: 'Upper leg',
      },
      {
        stepId: 3,
        description: 'ipsum lorem',
        treatmenttype: 'Broken_leg',
        bodylocation: 'Upper leg',
      },
      {
        stepId: 4,
        description: 'ipsum lorem',
        treatmenttype: 'Broken_leg',
        bodylocation: 'Upper leg',
      },
      {
        stepId: 5,
        description: 'ipsum lorem',
        treatmenttype: 'Post_Broken_Bone',
        bodylocation: 'Upper leg',
      },
    ];
  }
  openNewStep(gender) {
    const modalRef = this.modalService.open(NewStepComponent, { size: 'xl' });
    modalRef.componentInstance.gender = gender;
    modalRef.componentInstance.passEntry.subscribe((receivedEntry) => {
      console.log(receivedEntry);
      this.treatmentPlan.push(receivedEntry);
    });
  }
}
