import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import ImageMap from "image-map";

@Component({
  selector: 'app-new-step',
  templateUrl: './new-step.component.html',
  styleUrls: ['./new-step.component.css']
})
export class NewStepComponent implements OnInit {
  @Input() gender;
  @Output() passEntry: EventEmitter<any> = new EventEmitter();
  constructor(public activeModal: NgbActiveModal) { }
  newStep = {bodylocation:"", treatmenttype:"", description:""};
  passBack() {
    this.passEntry.emit(this.newStep);
  }
  setBodyLocation(location) {
    this.newStep.bodylocation = location;
  }
  ngOnInit(): void {
  }

}
